'use strict';

var request = require('@root/request');
request = require('util').promisify(request);

var defaults = {
	baseUrl: 'https://api.vultr.com/v1/dns'
};

module.exports.create = function(config) {
	var baseUrl = (config.baseUrl || defaults.baseUrl).replace(/\/$/, '');
	// In all of the other modules the API token is called token,
	// but here we support both.
	var apiKey = config.token || config.apiKey;

	function api(method, path, form) {
		return request({
			method: method,
			url: baseUrl + path,
			headers: {
				'API-Key': apiKey
			},
			json: true,
			form: form
		});
	}

	return {
		zones: function(data) {
			return api('GET', '/list').then(function(resp) {
				if (200 !== resp.statusCode) {
					console.error(resp.statusCode);
					console.error(resp.body);
					throw new Error('Could not get list of zones. Check api key, etc');
				}

				return resp.body.map(function(x) {
					return x.domain;
				});
			});
		},
		set: function(data) {
			var ch = data.challenge;
			var txt = ch.dnsAuthorization;
			// If the domain to be verified is

			//console.debug('adding txt', data);
			return api('POST', '/create_record', {
				type: 'TXT',
				name: ch.dnsPrefix,
				data: '"' + txt + '"', // vultr requires the TXT record wraped in quotes
				domain: ch.dnsZone,
				ttl: 300
			}).then(function(resp) {
				if (200 !== resp.statusCode) {
					console.error(resp.statusCode);
					console.error(resp.body);
					throw new Error('record did not set. check subdomain, api key, etc');
				}

				return true;
			});
		},
		remove: function(data) {
			var ch = data.challenge;

			return api('GET', '/records?domain=' + ch.dnsZone)
				.then(function(resp) {
					if (200 !== resp.statusCode) {
						throw new Error(
							'record did not set. check subdomain, api key, etc'
						);
					}

					resp = resp.body;
					//console.debug(resp);
					var entries = resp.filter(function(x) {
						return x.type === 'TXT';
					});

					var entry = entries.filter(function(x) {
						// vultr wraps the TXT record in double quotes
						return (
							x.data.substring(1, x.data.length - 1) === ch.dnsAuthorization
						);
					})[0];

					if (entry) {
						return entry.RECORDID;
					} else {
						throw new Error(
							"Couldn't remove record. check subdomain, api key, etc"
						);
					}
				})
				.then(function(recordId) {
					return api('POST', '/delete_record', {
						domain: ch.dnsZone,
						RECORDID: recordId
					}).then(function(resp) {
						if (200 !== resp.statusCode) {
							console.error(resp.statusCode);
							console.error(resp.body);
							throw new Error(
								'record did not remove. check subdomain, api key, etc'
							);
						}

						return true;
					});
				});
		},
		get: function(data) {
			var ch = data.challenge;
			//console.debug('getting txt', data);

			// Digital ocean provides the api to fetch records by ID. Since we do not have id, we fetch all the records,
			// filter the required TXT record

			return api('GET', '/records?domain=' + ch.dnsZone).then(function(resp) {
				resp = resp.body;

				var entries = resp.filter(function(x) {
					return x.type === 'TXT';
				});

				var entry = entries.filter(function(x) {
					// vultr wraps the TXT record in double quotes
					return x.data.substring(1, x.data.length - 1) === ch.dnsAuthorization;
				})[0];

				if (entry) {
					return {
						dnsAuthorization: entry.data.substring(1, entry.data.length - 1)
					};
				} else {
					return null;
				}
			});
		}
	};
};
